package com.gitlab.nickhutt.yearplanner.restapi.calendar;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import com.gitlab.nickhutt.yearplanner.domain.calendar.TemporalPoint;
import com.gitlab.nickhutt.yearplanner.restapi.model.EventDto;
import org.immutables.value.Value;
import org.springframework.lang.Nullable;

@Value.Immutable
public interface EventImplementation extends Event {

    @Override
    @Nullable
    String getId();

    @Override
    @Nullable
    String getTitle();

    @Override
    @Nullable
    String getDescription();

    @Override
    @Nullable
    TemporalPoint getStart();

    @Override
    @Nullable
    TemporalPoint getEnd();

    @Override
    @Nullable
    String getColor();

    static EventImplementation from(EventDto eventDto) {
        return ImmutableEventImplementation.builder()
                .title(eventDto.getTitle())
                .description(eventDto.getDescription())
                .start(TemporalPointImplementation.from(eventDto.getStart()))
                .end(TemporalPointImplementation.from(eventDto.getEnd()))
                .color(eventDto.getColor())
                .build();
    }
}
