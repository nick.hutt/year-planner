package com.gitlab.nickhutt.yearplanner.restapi.config;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

@Slf4j
public class CustomRequestLoggingFilter extends AbstractRequestLoggingFilter {

    private static final Set<String> EXCLUDED_URLS = Set.of("/actuator/health");

    @Override
    protected boolean shouldLog(HttpServletRequest request) {
        boolean exclude = EXCLUDED_URLS.contains(request.getRequestURI());
        return !exclude && log.isDebugEnabled();
    }

    /**
     * Writes a log message before the request is processed.
     */
    @Override
    protected void beforeRequest(HttpServletRequest request, String message) {
        log.debug(message);
    }

    /**
     * Writes a log message after the request is processed.
     */
    @Override
    protected void afterRequest(HttpServletRequest request, String message) {
        log.debug(message);
    }

}
