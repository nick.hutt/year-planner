package com.gitlab.nickhutt.yearplanner.restapi.calendar;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import com.gitlab.nickhutt.yearplanner.domain.calendar.TemporalPoint;
import com.gitlab.nickhutt.yearplanner.restapi.model.EventDto;
import com.gitlab.nickhutt.yearplanner.restapi.model.TemporalPointDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class EventConverter implements Converter<Event, EventDto> {

    @Override
    public EventDto convert(Event event) {
        return new EventDto()
                .id(event.getId())
                .title(event.getTitle())
                .description(event.getDescription())
                .start(convertTemporalPoint(event.getStart()))
                .end(convertTemporalPoint(event.getEnd()))
                .color(event.getColor());
    }

    private TemporalPointDto convertTemporalPoint(TemporalPoint temporalPoint) {
        return new TemporalPointDto()
                .date(temporalPoint.getDate())
                .datetime(temporalPoint.getDatetime());
    }

}