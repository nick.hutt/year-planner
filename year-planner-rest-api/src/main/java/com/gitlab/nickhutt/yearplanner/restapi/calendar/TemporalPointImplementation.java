package com.gitlab.nickhutt.yearplanner.restapi.calendar;

import com.gitlab.nickhutt.yearplanner.domain.calendar.TemporalPoint;
import com.gitlab.nickhutt.yearplanner.restapi.model.TemporalPointDto;
import org.immutables.value.Value;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.time.OffsetDateTime;

@Value.Immutable
public interface TemporalPointImplementation extends TemporalPoint {

    @Override
    @Nullable
    LocalDate getDate();

    @Override
    @Nullable
    OffsetDateTime getDatetime();

    static TemporalPoint from(TemporalPointDto temporalPointDto) {
        return ImmutableTemporalPointImplementation.builder()
                .date(temporalPointDto.getDate())
                .datetime(temporalPointDto.getDatetime())
                .build();
    }
}
