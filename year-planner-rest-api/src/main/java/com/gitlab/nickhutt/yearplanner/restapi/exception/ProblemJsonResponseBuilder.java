package com.gitlab.nickhutt.yearplanner.restapi.exception;

import org.immutables.value.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ProblemJsonResponseBuilder {

    public ResponseEntity<ProblemJson> buildResponseEntity(HttpStatus httpStatus,
                                                           String title,
                                                           String detail) {
        ProblemJson problemJson = ImmutableProblemJson
                .builder()
                .title(title)
                .detail(detail)
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PROBLEM_JSON);
        return new ResponseEntity<>(problemJson, headers, httpStatus);
    }

    @Value.Immutable
    interface ProblemJson {
        String getTitle();
        String getDetail();
    }

}
