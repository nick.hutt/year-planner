package com.gitlab.nickhutt.yearplanner.restapi.user;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.gitlab.nickhutt.yearplanner.domain.user.User;
import com.gitlab.nickhutt.yearplanner.restapi.model.UserDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserConverter implements Converter<User, UserDto> {

    @Override
    public UserDto convert(User user) {
        return new UserDto()
                .username(user.getUsername())
                .userId(user.getId())
                .calendarIds(convertCalendarIdsToList(user));
    }

    List<String> convertCalendarIdsToList(User user) {
        return user.getCalendars().stream()
                .map(Calendar::getId)
                .collect(Collectors.toList());
    }

}