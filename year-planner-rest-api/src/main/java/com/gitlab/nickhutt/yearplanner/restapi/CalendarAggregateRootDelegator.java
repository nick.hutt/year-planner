package com.gitlab.nickhutt.yearplanner.restapi;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.gitlab.nickhutt.yearplanner.domain.calendar.CalendarAggregateRoot;
import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import com.gitlab.nickhutt.yearplanner.domain.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

// TODO find a better way of delegating
@Primary
@Service
@Slf4j
public class CalendarAggregateRootDelegator implements CalendarAggregateRoot {

    @Qualifier("google")
    @Autowired
    CalendarAggregateRoot googleCalendarAggregateRoot;

    @Qualifier("internal")
    @Autowired
    CalendarAggregateRoot internalAggregateRoot;

    private CalendarAggregateRoot aggregateRoot(User user) {
        if (user.getThirdPartyAccess() == null) {
            log.info("Choosing internal");
            return internalAggregateRoot;
        } else {
            log.info("Choosing google");
            return googleCalendarAggregateRoot;
        }
    }

    @Override
    public Calendar findById(User user, String calendarId) {
        return aggregateRoot(user).findById(user, calendarId);
    }

    @Override
    public Calendar addEvent(User user, String calendarId, Event event) {
        return aggregateRoot(user).addEvent(user, calendarId, event);
    }

    @Override
    public Calendar updateEvent(User user, String calendarId, String eventId, Event event) {
        return aggregateRoot(user).updateEvent(user, calendarId, eventId, event);
    }

    @Override
    public Calendar deleteEvent(User user, String calendarId, String eventId) {
        return aggregateRoot(user).deleteEvent(user, calendarId, eventId);
    }
}
