package com.gitlab.nickhutt.yearplanner.restapi.calendar;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.gitlab.nickhutt.yearplanner.restapi.model.CalendarDto;
import com.gitlab.nickhutt.yearplanner.restapi.model.EventDto;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CalendarConverter implements Converter<Calendar, CalendarDto> {

    private final EventConverter eventConverter;

    @Override
    public CalendarDto convert(Calendar calendar) {
        List<EventDto> events = calendar.getEvents()
                .stream()
                .map(event -> eventConverter.convert(event))
                .collect(Collectors.toList());
        return new CalendarDto()
                .id(calendar.getId())
                .name(calendar.getName())
                .events(events);
    }

}