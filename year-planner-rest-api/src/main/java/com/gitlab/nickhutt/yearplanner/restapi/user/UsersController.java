package com.gitlab.nickhutt.yearplanner.restapi.user;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.gitlab.nickhutt.yearplanner.domain.calendar.CalendarAggregateRoot;
import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import com.gitlab.nickhutt.yearplanner.domain.user.AuthenticatedUserRepository;
import com.gitlab.nickhutt.yearplanner.domain.user.User;
import com.gitlab.nickhutt.yearplanner.domain.user.UserAggregateRoot;
import com.gitlab.nickhutt.yearplanner.restapi.api.UsersApi;
import com.gitlab.nickhutt.yearplanner.restapi.calendar.EventImplementation;
import com.gitlab.nickhutt.yearplanner.restapi.model.CalendarDto;
import com.gitlab.nickhutt.yearplanner.restapi.model.EventDto;
import com.gitlab.nickhutt.yearplanner.restapi.model.UserCreationDto;
import com.gitlab.nickhutt.yearplanner.restapi.model.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class UsersController implements UsersApi {

    private final ConversionService conversionService;

    private final UserAggregateRoot userAggregateRoot;

    private final AuthenticatedUserRepository authenticatedUserRepository;

    private final CalendarAggregateRoot calendarAggregateRoot;

    @Override
    public ResponseEntity<UserDto> getUser(String username) {
        User user = userAggregateRoot.findByUsername(username);
        return ResponseEntity.ok(conversionService.convert(user, UserDto.class));
    }

    @Override
    public ResponseEntity<UserDto> createUser(UserCreationDto userCreationDto) {
        User user = authenticatedUserRepository.findUserFromCode(userCreationDto.getCode());
        return ResponseEntity.ok(conversionService.convert(user, UserDto.class));
    }

    @Override
    public ResponseEntity<CalendarDto> getCalendar(String calendarId, String userId) {
        User user = userAggregateRoot.findById(userId);
        Calendar calendar = calendarAggregateRoot.findById(user, calendarId);
        return ResponseEntity.ok(conversionService.convert(calendar, CalendarDto.class));
    }

    @Override
    public ResponseEntity<CalendarDto> createEvent(String calendarId, String userId, EventDto eventDto) {
        User user = userAggregateRoot.findById(userId);
        Event event = EventImplementation.from(eventDto);
        Calendar calendar = calendarAggregateRoot.addEvent(user, calendarId, event);
        return ResponseEntity.ok(conversionService.convert(calendar, CalendarDto.class));
    }

    @Override
    public ResponseEntity<CalendarDto> updateEvent(String calendarId, String eventId, String userId, EventDto eventDto) {
        User user = userAggregateRoot.findById(userId);
        Event event = EventImplementation.from(eventDto);
        Calendar calendar = calendarAggregateRoot.updateEvent(user, calendarId, eventId, event);
        return ResponseEntity.ok(conversionService.convert(calendar, CalendarDto.class));
    }

    @Override
    public ResponseEntity<CalendarDto> deleteEvent(String calendarId, String eventId, String userId) {
        User user = userAggregateRoot.findById(userId);
        Calendar calendar = calendarAggregateRoot.deleteEvent(user, calendarId, eventId);
        return ResponseEntity.ok(conversionService.convert(calendar, CalendarDto.class));
    }

}
