package com.gitlab.nickhutt.yearplanner.restapi.exception;

import com.gitlab.nickhutt.yearplanner.domain.NoSuchEntityException;
import com.gitlab.nickhutt.yearplanner.domain.UnableToAuthenticateUserException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private final ProblemJsonResponseBuilder problemJsonResponseBuilder;

    @ExceptionHandler(value = {NoSuchEntityException.class})
    protected ResponseEntity<ProblemJsonResponseBuilder.ProblemJson> handleNoSuchEntityException(
            RuntimeException e,
            WebRequest request) {
        log.warn("Entity not found: {}", e.getMessage());
        return problemJsonResponseBuilder.buildResponseEntity(HttpStatus.NOT_FOUND,
                "Entity not found",
                e.getMessage());
    }

    @ExceptionHandler(value = {UnableToAuthenticateUserException.class})
    protected ResponseEntity<ProblemJsonResponseBuilder.ProblemJson> handleUnableToAuthenticateUserException(
            RuntimeException e,
            WebRequest request) {
        log.warn("Unable to authenticate: {}", e.getMessage());
        return problemJsonResponseBuilder.buildResponseEntity(HttpStatus.BAD_REQUEST,
                "Unable to authenticate user",
                e.getMessage());
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<ProblemJsonResponseBuilder.ProblemJson> handleGeneralException(
            RuntimeException e,
            WebRequest request) {
        log.error("Internal error occurred", e);
        return problemJsonResponseBuilder.buildResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR,
                "Internal server error",
                "An internal error occurred, this has been logged");
    }


}
