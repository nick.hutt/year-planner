---
openapi: 3.0.2
info:
  title: Year Planner
  version: 0.0.1
paths:
  /users/{username}:
    summary: Path used to manage a single user.
    description: ""
    get:
      operationId: getUser
      summary: Get a user
      description: Gets the details of a single instance of a `user`.
      responses:
        200:
          description: Successful response - returns a single `user`.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/user'
    parameters:
      - name: username
        in: path
        description: A unique identifier for a `user`.
        required: true
        schema:
          type: string
  /users/:
    post:
      summary: Creates a new user
      operationId: createUser
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/userCreation'
        required: true
      responses:
        200:
          description: Successfully created a user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/user'
  /users/{userId}/calendars/{calendarId}/events/:
    post:
      summary: Create a new event.
      operationId: createEvent
      requestBody:
        description: Create a new `event`.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/event'
        required: true
      responses:
        201:
          description: Successfully created.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/calendar'
    parameters:
      - name: calendarId
        in: path
        required: true
        schema:
          type: string
      - name: userId
        in: path
        required: true
        schema:
          type: string
  /users/{userId}/calendars/{calendarId}:
    summary: Path used to manage a single calendar.
    description: ""
    get:
      summary: Get a calendar
      operationId: getCalendar
      description: Gets the details of a single instance of a `calendar`.
      responses:
        200:
          description: Successful response - returns a single `calendar`.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/calendar'
    parameters:
      - name: calendarId
        in: path
        description: A unique identifier for a `calendar`.
        required: true
        schema:
          type: string
      - name: userId
        in: path
        required: true
        schema:
          type: string
  /users/{userId}/calendars/{calendarId}/events/{eventId}:
    put:
      summary: Update/replace an event.
      operationId: updateEvent
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/event'
        required: true
      responses:
        200:
          description: Successfully updated.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/calendar'
    delete:
      summary: Delete an event.
      operationId: deleteEvent
      responses:
        200:
          description: Successfully deleted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/calendar'
    parameters:
      - name: calendarId
        in: path
        required: true
        schema:
          type: string
      - name: eventId
        in: path
        required: true
        schema:
          type: string
      - name: userId
        in: path
        required: true
        schema:
          type: string
components:
  schemas:
    calendar:
      title: Root Type for calendar
      description: The root of the calendar type's schema.
      type: object
      properties:
        name:
          type: string
        events:
          type: array
          items:
            $ref: '#/components/schemas/event'
        id:
          type: string
    event:
      title: Root Type for event
      description: The root of the event type's schema.
      type: object
      properties:
        id:
          type: string
        title:
          type: string
        description:
          type: string
        start:
          $ref: '#/components/schemas/temporalPoint'
          properties:
            datetime:
              format: date-time
              type: string
            date:
              format: date
              type: string
        end:
          $ref: '#/components/schemas/temporalPoint'
          properties:
            datetime:
              format: date-time
              type: string
            date:
              format: date
              type: string
        color:
          type: string
    user:
      title: Root Type for user
      description: The root of the user type's schema.
      required:
        - calendarIds
        - userId
      type: object
      properties:
        username:
          type: string
        calendarIds:
          type: array
          items:
            type: string
        userId:
          type: string
    temporalPoint:
      title: Root Type for temporalPoint
      description: The root of the temporalPoint type's schema. One and only one of
        date and datetime should be supplied.
      type: object
      properties:
        datetime:
          format: date-time
          type: string
        date:
          format: date
          type: string
    userCreation:
      title: Root Type for user-creation
      description: The root of the user-creation type's schema.
      type: object
      properties:
        code:
          type: string
      example: |-
        {
            "code": "3heu23g43h48ryw3ihdqwi23h2i4hi4324"
        }
