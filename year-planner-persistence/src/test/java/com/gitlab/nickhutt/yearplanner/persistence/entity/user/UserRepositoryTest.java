package com.gitlab.nickhutt.yearplanner.persistence.entity.user;

import com.gitlab.nickhutt.yearplanner.domain.user.User;
import com.gitlab.nickhutt.yearplanner.persistence.TestApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ContextConfiguration(classes = {UserRepository.class, TestApplication.class, UserEntityFactory.class})
class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    void shouldPersistAndRetrieveUser() {
        // given
        UserEntity sut = new UserEntity();
        sut.setUsername("username");
        entityManager.persist(sut);
        entityManager.flush();

        // when
        User found = userRepository.findByUsername(sut.getUsername());

        // then
        assertEquals(found.getUsername(), sut.getUsername());
    }


    @Test
    void shouldCreateAndRetrieveUser() {
        // given
        String username = "username";
        String accessToken = "accessToken";
        String thirdPartyAccessTitle = "thirdPartyAccessTitle";
        User created = userRepository.create(username, accessToken, thirdPartyAccessTitle);

        // when
        User found = userRepository.findByUsername(username);

        // then
        assertEquals(found.getUsername(), username);
        assertEquals(found.getThirdPartyAccess().getAccessToken(), accessToken);
        assertEquals(found.getThirdPartyAccess().getThirdPartyTitle(), thirdPartyAccessTitle);
    }
}