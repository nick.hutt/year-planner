package com.gitlab.nickhutt.yearplanner.persistence.entity.calendar;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import com.gitlab.nickhutt.yearplanner.persistence.TestApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ContextConfiguration(classes = {EventEntityFactory.class, CalendarRepository.class, TestApplication.class})
class CalendarRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CalendarRepository calendarRepository;

    @Test
    void shouldPersistAndRetrieveCalendar() {
        // when
        CalendarEntity sut = persistCalendar();

        // then
        Calendar found = calendarRepository.findById(null, sut.getId());
        assertEquals(found.getName(), sut.getName());
    }

    @Test
    void shouldAddEvent() {
        CalendarEntity sut = persistCalendar();
        assertEquals(sut.getEvents().size(), 1);

        // when
        EventEntity eventToPersist = createEventEntity("new event");
        calendarRepository.addEvent(null, sut.getId(), eventToPersist);

        // then
        Calendar found = calendarRepository.findById(null, sut.getId());
        assertEquals(found.getEvents().size(), 2);
        Event eventReloaded = found.getEvents().get(1);
        assertEquals(eventToPersist.getTitle(), eventReloaded.getTitle());
        assertEquals(eventToPersist.getDescription(), eventReloaded.getDescription());
        assertEquals(eventToPersist.getColor(), eventReloaded.getColor());
    }

    @Test
    void shouldUpdateEvent() {
        CalendarEntity sut = persistCalendar();
        assertEquals(sut.getEvents().size(), 1);

        // when
        Event firstEvent = sut.getEvents().get(0);
        EventEntity updatedEvent = new EventEntity();
        updatedEvent.setTitle("updated event");
        updatedEvent.setDescription("updated description");
        updatedEvent.setColor("updated color");
        updatedEvent.setStart(new TemporalPointValueObject());
        updatedEvent.setEnd(new TemporalPointValueObject());
        calendarRepository.updateEvent(null, sut.getId(), firstEvent.getId(), updatedEvent);

        // then
        Calendar found = calendarRepository.findById(null, sut.getId());
        Event eventReloaded = found.getEvents().get(0);
        assertEquals(updatedEvent.getTitle(), eventReloaded.getTitle());
        assertEquals(updatedEvent.getDescription(), eventReloaded.getDescription());
        assertEquals(updatedEvent.getColor(), eventReloaded.getColor());
    }

    CalendarEntity persistCalendar() {
        CalendarEntity sut = new CalendarEntity();
        sut.setName("calendar");

        EventEntity eventEntity = createEventEntity("initial event");
        eventEntity.setCalendar(sut);
        sut.getEventEntities().add(eventEntity);

        entityManager.persist(sut);
        entityManager.flush();
        return sut;
    }

    EventEntity createEventEntity(String title) {
        EventEntity eventEntity = new EventEntity();
        eventEntity.setTitle(title);
        eventEntity.setDescription("description");
        eventEntity.setStart(new TemporalPointValueObject());
        eventEntity.setEnd(new TemporalPointValueObject());
        eventEntity.setColor("#ff0000");
        return eventEntity;
    }

}