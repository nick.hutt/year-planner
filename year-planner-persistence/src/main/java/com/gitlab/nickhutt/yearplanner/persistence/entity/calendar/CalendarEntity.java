package com.gitlab.nickhutt.yearplanner.persistence.entity.calendar;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import com.gitlab.nickhutt.yearplanner.persistence.entity.BaseEntity;
import com.gitlab.nickhutt.yearplanner.persistence.entity.user.UserEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "calendar")
public class CalendarEntity extends BaseEntity implements Calendar {

    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "calendar")
    private List<EventEntity> events = new ArrayList<>();

    @Override
    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public UserEntity getUser() {
        return user;
    }

    void setUser(UserEntity user) {
        this.user = user;
    }

    @Override
    public List<Event> getEvents() {
        List<Event> eventList = events.stream()
                .map(event -> (Event) event)
                .collect(Collectors.toList());
        return Collections.unmodifiableList(eventList);
    }

    List<EventEntity> getEventEntities() {
        return events;
    }

    void setEvents(List<EventEntity> events) {
        this.events = events;
    }
}
