package com.gitlab.nickhutt.yearplanner.persistence.entity.user;

import org.springframework.stereotype.Service;

@Service
class UserEntityFactory {
    UserEntity from(String username, String accessToken, String thirdPartyAccessTitle) {
        UserEntity user = new UserEntity();
        user.setUsername(username);
        ThirdPartyAccessEntity thirdPartyAccess = new ThirdPartyAccessEntity();
        thirdPartyAccess.setAccessToken(accessToken);
        thirdPartyAccess.setThirdPartyTitle(thirdPartyAccessTitle);
        thirdPartyAccess.setUser(user);
        user.setThirdPartyAccess(thirdPartyAccess);
        return user;
    }

}
