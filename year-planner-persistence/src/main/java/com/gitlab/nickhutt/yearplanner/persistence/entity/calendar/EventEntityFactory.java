package com.gitlab.nickhutt.yearplanner.persistence.entity.calendar;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import org.springframework.stereotype.Service;

@Service
class EventEntityFactory {
    EventEntity from(Event event) {
        EventEntity eventEntity = new EventEntity();
        copyEntity(event, eventEntity);
        return eventEntity;
    }

    void copyEntity(Event from, EventEntity to) {
        to.setTitle(from.getTitle());
        to.setDescription(from.getDescription());
        to.setStart(TemporalPointValueObject.from(from.getStart()));
        to.setEnd(TemporalPointValueObject.from(from.getEnd()));
        to.setColor(from.getColor());
    }
}
