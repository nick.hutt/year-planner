package com.gitlab.nickhutt.yearplanner.persistence.entity.user;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.gitlab.nickhutt.yearplanner.domain.user.ThirdPartyAccess;
import com.gitlab.nickhutt.yearplanner.domain.user.User;
import com.gitlab.nickhutt.yearplanner.persistence.entity.BaseEntity;
import com.gitlab.nickhutt.yearplanner.persistence.entity.calendar.CalendarEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "usertable")
public class UserEntity extends BaseEntity implements User {

    private String username;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER, mappedBy = "user")
    private List<CalendarEntity> calendars = new ArrayList<>();

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL)
    private ThirdPartyAccessEntity thirdPartyAccess;

    protected UserEntity() {}

    @Override
    public String getUsername() {
        return username;
    }

    void setUsername(String username) {
        this.username = username;
    }

    @Override
    public List<Calendar> getCalendars() {
        List<Calendar> calendarList = calendars.stream()
                .map(calendar -> (Calendar) calendar)
                .collect(Collectors.toList());
        return Collections.unmodifiableList(calendarList);
    }

    void setCalendars(List<CalendarEntity> calendars) {
        this.calendars = calendars;
    }

    @Override
    public ThirdPartyAccess getThirdPartyAccess() {
        return thirdPartyAccess;
    }

    public void setThirdPartyAccess(ThirdPartyAccessEntity thirdPartyAccess) {
        this.thirdPartyAccess = thirdPartyAccess;
    }
}
