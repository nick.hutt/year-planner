package com.gitlab.nickhutt.yearplanner.persistence.entity.calendar;

import org.springframework.data.repository.CrudRepository;

interface CalendarCrudRepository extends CrudRepository<CalendarEntity, String> {
}
