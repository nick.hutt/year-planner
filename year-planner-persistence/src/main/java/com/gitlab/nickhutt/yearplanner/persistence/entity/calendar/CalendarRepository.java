package com.gitlab.nickhutt.yearplanner.persistence.entity.calendar;

import com.gitlab.nickhutt.yearplanner.domain.NoSuchEntityException;
import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.gitlab.nickhutt.yearplanner.domain.calendar.CalendarAggregateRoot;
import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import com.gitlab.nickhutt.yearplanner.domain.user.User;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("internal")
@RequiredArgsConstructor
@Slf4j
class CalendarRepository implements CalendarAggregateRoot {

    private final EntityManager entityManager;

    private final EventEntityFactory eventEntityFactory;

    private final CalendarCrudRepository calendarCrudRepository;

    @Override
    public CalendarEntity findById(User user, String calendarId) {
        return calendarCrudRepository.findById(calendarId)
                .orElseThrow(() -> new NoSuchEntityException(CalendarEntity.class, calendarId));
    }

    @Transactional
    @Override
    public Calendar addEvent(User user, String calendarId, Event event) {
        CalendarEntity calendar = findById(user, calendarId);
        EventEntity eventEntity = eventEntityFactory.from(event);
        eventEntity.setCalendar(calendar);
        calendar.getEventEntities().add(eventEntity);
        entityManager.persist(calendar);
        return calendar;
    }

    @Transactional
    @Override
    public Calendar updateEvent(User user, String calendarId, String eventId, Event event) {
        CalendarEntity calendar = findById(user, calendarId);
        EventEntity existingEvent = findEvent(calendar, eventId);
        eventEntityFactory.copyEntity(event, existingEvent);
        entityManager.persist(calendar);
        return calendar;
    }

    @Transactional
    @Override
    public Calendar deleteEvent(User user, String calendarId, String eventId) {
        CalendarEntity calendar = findById(user, calendarId);
        EventEntity event = findEvent(calendar, eventId);
        log.info("Found event: {}", event.getId());
        calendar.getEventEntities().remove(event);
        entityManager.persist(calendar);
        return calendar;
    }

    private EventEntity findEvent(CalendarEntity calendarEntity, String eventId) {
        return calendarEntity.getEventEntities()
                .stream()
                .filter(matchEvent -> matchEvent.getId().equals(eventId))
                .findFirst()
                .orElseThrow(() -> new NoSuchEntityException("Event ID: " + eventId + " not found"));
    }

}
