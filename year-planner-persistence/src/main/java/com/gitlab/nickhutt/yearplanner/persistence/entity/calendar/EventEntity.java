package com.gitlab.nickhutt.yearplanner.persistence.entity.calendar;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import com.gitlab.nickhutt.yearplanner.persistence.entity.BaseEntity;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "event")
public class EventEntity extends BaseEntity implements Event {

    String title;

    String description;

    @ManyToOne
    @JoinColumn(name = "calendar_id")
    private CalendarEntity calendar;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="datetime", column=@Column(name="startdatetime")),
            @AttributeOverride(name="date", column=@Column(name="startdate"))
    })
    TemporalPointValueObject start;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="datetime", column=@Column(name="enddatetime")),
            @AttributeOverride(name="date", column=@Column(name="enddate"))
    })
    TemporalPointValueObject end;

    String color;

    @Override
    public String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    void setDescription(String description) {
        this.description = description;
    }

    @Override
    public TemporalPointValueObject getStart() {
        return start;
    }

    void setStart(TemporalPointValueObject start) {
        this.start = start;
    }

    @Override
    public TemporalPointValueObject getEnd() {
        return end;
    }

    void setEnd(TemporalPointValueObject end) {
        this.end = end;
    }

    @Override
    public String getColor() {
        return color;
    }

    void setColor(String color) {
        this.color = color;
    }

    void setCalendar(CalendarEntity calendar) {
        this.calendar = calendar;
    }
}
