package com.gitlab.nickhutt.yearplanner.persistence.entity.calendar;

import com.gitlab.nickhutt.yearplanner.domain.calendar.TemporalPoint;
import jakarta.persistence.Embeddable;
import java.time.LocalDate;
import java.time.OffsetDateTime;

@Embeddable
public class TemporalPointValueObject implements TemporalPoint {
    private OffsetDateTime datetime;
    private LocalDate date;

    @Override
    public OffsetDateTime getDatetime() {
        return datetime;
    }

    void setDatetime(OffsetDateTime datetime) {
        this.datetime = datetime;
    }

    @Override
    public LocalDate getDate() {
        return date;
    }

    void setDate(LocalDate date) {
        this.date = date;
    }

    public static TemporalPointValueObject from(TemporalPoint temporalPoint) {
        TemporalPointValueObject out = new TemporalPointValueObject();
        out.setDate(temporalPoint.getDate());
        out.setDatetime(temporalPoint.getDatetime());
        return out;
    }
}
