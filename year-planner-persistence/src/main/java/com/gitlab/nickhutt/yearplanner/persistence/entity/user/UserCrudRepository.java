package com.gitlab.nickhutt.yearplanner.persistence.entity.user;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

interface UserCrudRepository extends CrudRepository<UserEntity, String> {
    Optional<UserEntity> findByUsername(String username);
}
