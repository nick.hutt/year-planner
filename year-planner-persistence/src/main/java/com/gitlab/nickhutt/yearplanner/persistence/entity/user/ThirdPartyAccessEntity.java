package com.gitlab.nickhutt.yearplanner.persistence.entity.user;

import com.gitlab.nickhutt.yearplanner.domain.user.ThirdPartyAccess;
import com.gitlab.nickhutt.yearplanner.persistence.entity.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "thirdpartyaccess")
public class ThirdPartyAccessEntity extends BaseEntity implements ThirdPartyAccess {

    private String thirdPartyTitle;

    private String accessToken;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    protected ThirdPartyAccessEntity() {
    }

    @Override
    public String getThirdPartyTitle() {
        return thirdPartyTitle;
    }

    public void setThirdPartyTitle(String thirdPartyTitle) {
        this.thirdPartyTitle = thirdPartyTitle;
    }

    @Override
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
