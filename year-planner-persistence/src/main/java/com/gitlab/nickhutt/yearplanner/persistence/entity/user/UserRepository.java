package com.gitlab.nickhutt.yearplanner.persistence.entity.user;

import com.gitlab.nickhutt.yearplanner.domain.NoSuchEntityException;
import com.gitlab.nickhutt.yearplanner.domain.user.User;
import com.gitlab.nickhutt.yearplanner.domain.user.UserAggregateRoot;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
class UserRepository implements UserAggregateRoot {

    private final EntityManager entityManager;

    private final UserCrudRepository userCrudRepository;

    private final UserEntityFactory userEntityFactory;

    @Override
    public User findByUsername(String username) {
        return userCrudRepository.findByUsername(username)
                .orElseThrow(() -> new NoSuchEntityException("Cannot find user: " + username));
    }

    @Override
    public User findById(String id) {
        return userCrudRepository.findById(id)
                .orElseThrow(() -> new NoSuchEntityException("Cannot find user with id: " + id));
    }

    @Transactional
    @Override
    public User create(String username, String accessToken, String thirdPartyAccessTitle) {
        UserEntity user = userEntityFactory.from(username, accessToken, thirdPartyAccessTitle);
        entityManager.persist(user);
        return user;
    }


}
