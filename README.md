# Year Planner

Year planner is a Spring Boot 3 demo project to showcase and practice a few typical functions.

* DDD - there is a domain module intended to capture the domain methods, broadly analogous to a service layer.
* Persistence layer using Spring Data.
* Adapter - connection to Google apps (calendar).
* Rest APIs interfaces generated from Open API specification (API design first approach).

## Inversion of control

Inversion of control (the `I` from SOLID) via dependency injection. This leads to the following structure:

![Diagram that shows the module structure](docs/year-planner.drawio.png "Module structure")

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Java JDK version 21
* Maven
* Docker

### Google Calendar integration

Info on how to configure:

https://docs.simplecalendar.io/gcal-pro-how-to-properly-configure-the-google-oauth-client/

Add the Google app credentials via the `docker-compose.override.yml`:

```
services:
  year-planner:
    environment:
      GOOGLE-ACCESS-TOKEN_CLIENT-ID: <client-id>
      GOOGLE-ACCESS-TOKEN_CLIENT-SECRET: <secret>
      GOOGLE-ACCESS-TOKEN_REDIRECT-URI: <redirect>
      GOOGLE-CLIENT_APPLICATION-NAME: <name>
```

### Building

```
maven clean install
```

## Docker

Build as above and then:

```
docker compose up
```

## Versioning

Use [SemVer](http://semver.org/) for versioning. For the versions available.

## License

All rights reserved.
