package com.gitlab.nickhutt.yearplanner.domain.calendar;

public interface Event {
    String getId();
    String getTitle();
    String getDescription();
    TemporalPoint getStart();
    TemporalPoint getEnd();
    String getColor();
}
