package com.gitlab.nickhutt.yearplanner.domain.user;

public interface ThirdPartyAccess {
    String getThirdPartyTitle();
    String getAccessToken();
}
