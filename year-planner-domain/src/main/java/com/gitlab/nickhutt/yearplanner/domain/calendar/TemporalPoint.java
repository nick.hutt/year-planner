package com.gitlab.nickhutt.yearplanner.domain.calendar;

import java.time.LocalDate;
import java.time.OffsetDateTime;

public interface TemporalPoint {
    OffsetDateTime getDatetime();
    LocalDate getDate();
}
