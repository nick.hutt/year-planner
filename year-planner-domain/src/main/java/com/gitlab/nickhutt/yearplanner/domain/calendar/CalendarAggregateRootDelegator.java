package com.gitlab.nickhutt.yearplanner.domain.calendar;

public interface CalendarAggregateRootDelegator {
    Calendar findById(String calendarId);
    Calendar addEvent(String calendarId, Event event);
    Calendar updateEvent(String calendarId, String eventId, Event event);
    Calendar deleteEvent(String calendarId, String eventId);
}
