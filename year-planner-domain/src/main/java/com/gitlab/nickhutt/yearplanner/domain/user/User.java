package com.gitlab.nickhutt.yearplanner.domain.user;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;

import java.util.List;

public interface User {
    String getId();
    String getUsername();
    List<Calendar> getCalendars();
    ThirdPartyAccess getThirdPartyAccess();
}
