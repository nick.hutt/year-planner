package com.gitlab.nickhutt.yearplanner.domain.calendar;

import java.util.List;

public interface Calendar {
    String getId();
    String getName();
    List<Event> getEvents();
}
