package com.gitlab.nickhutt.yearplanner.domain;

public class UnableToAuthenticateUserException extends RuntimeException {
    public UnableToAuthenticateUserException(String message) {
        super(message);
    }
}
