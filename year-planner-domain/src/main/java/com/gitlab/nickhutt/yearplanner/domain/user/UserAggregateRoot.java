package com.gitlab.nickhutt.yearplanner.domain.user;

public interface UserAggregateRoot {
    User findByUsername(String username);
    User findById(String id);
    User create(String username, String accessToken, String thirdPartyAccessTitle);
}
