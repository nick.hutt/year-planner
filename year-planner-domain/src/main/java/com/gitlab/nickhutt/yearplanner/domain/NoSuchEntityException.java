package com.gitlab.nickhutt.yearplanner.domain;

public class NoSuchEntityException extends RuntimeException {
    public NoSuchEntityException(Class<?> clazz, String id) {
        super("Unable to find " + clazz.getSimpleName() + " with id " + id);
    }

    public NoSuchEntityException(String message) {
        super(message);
    }
}
