package com.gitlab.nickhutt.yearplanner.domain.user;

public interface AuthenticatedUserRepository {
    User findUserFromCode(String code);
}
