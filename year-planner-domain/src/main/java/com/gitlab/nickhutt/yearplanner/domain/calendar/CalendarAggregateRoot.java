package com.gitlab.nickhutt.yearplanner.domain.calendar;

import com.gitlab.nickhutt.yearplanner.domain.user.User;

public interface CalendarAggregateRoot {
    Calendar findById(User user, String calendarId);
    Calendar addEvent(User user, String calendarId, Event event);
    Calendar updateEvent(User user, String calendarId, String eventId, Event event);
    Calendar deleteEvent(User user, String calendarId, String eventId);
}
