package com.gitlab.nickhutt.yearplanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YearPlannerApplication {
	public static void main(String[] args) {
		SpringApplication.run(YearPlannerApplication.class, args);
	}

}
