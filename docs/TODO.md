[x] - Remove commons-logging.jar
[x] - Test Spring Boot 3.2 version
[x] - Fix warnings
[x] - Remove star in library inserts
[x] - Extract db username and password to env variables
[x] - Add information about the project
[x] - Use slf4j logger everywhere
[ ] - Replace TODO for Oauth user? Use Spring Boot 3 built in Oauth