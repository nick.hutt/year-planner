package com.gitlab.nickhutt.yearplanner.googlecalendar.accesstoken;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class GoogleAccessTokenClientTest {

    GoogleAccessTokenClient cut = new GoogleAccessTokenClient(null);

    @Disabled("This needs a new access token each run")
    @Test
    void getAccessTokenFromCode2() {
        String accessToken = cut.getAccessTokenFromCode("4/ZgHPFlVpWD2lJkU0f-RVNRkU0uTAgYzaUeqsFKtHVU4ewWQRTempv3vlZynt_JUmnYc2kG5ajtkTT34kvZpNYbk");
        assertNotNull(accessToken);
    }
}