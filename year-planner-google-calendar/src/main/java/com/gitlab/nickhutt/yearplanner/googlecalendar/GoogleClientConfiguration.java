package com.gitlab.nickhutt.yearplanner.googlecalendar;

import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

// TODO this configuration should be in this module
@Configuration
public class GoogleClientConfiguration {

    @Value("${google-access-token.url}")
    @NotNull
    private String url;

    @Value("${google-access-token.client-id}")
    @NotNull
    private String clientId;

    @Value("${google-access-token.client-secret}")
    @NotNull
    private String clientSecret;

    @Value("${google-access-token.redirect-uri}")
    @NotNull
    private String redirectUri;

    @Value("${google-client.application-name}")
    @NotNull
    private String applicationName;

    public String getUrl() {
        return url;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public String getApplicationName() {
        return applicationName;
    }
}
