package com.gitlab.nickhutt.yearplanner.googlecalendar.calendar;

import com.gitlab.nickhutt.yearplanner.domain.InternalException;
import com.gitlab.nickhutt.yearplanner.domain.user.User;
import com.gitlab.nickhutt.yearplanner.googlecalendar.GoogleClientConfiguration;
import com.gitlab.nickhutt.yearplanner.googlecalendar.model.GoogleCalendar;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.Events;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GoogleCalendarClient {

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private final GoogleClientConfiguration configuration;

    private Calendar createService(String accessToken) {
        try {
            NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
            return new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                    .setApplicationName(configuration.getApplicationName())
                    .build();
        } catch (Exception e) {
            throw new InternalException("Problem using calendar service", e);
        }
    }

    public List<GoogleCalendar> getCalendarIds(User user) {
        Calendar service = createService(user.getThirdPartyAccess().getAccessToken());

        CalendarList list;
        try {
            list = service.calendarList().list().execute();
        } catch (Exception e) {
            throw new InternalException("Problem retrieving calendar Ids", e);
        }

        return list.getItems().stream()
                .map(GoogleCalendar::from)
                .collect(Collectors.toList());
    }

    public GoogleCalendar findById(User user, String calendarId) {
        Calendar service = createService(user.getThirdPartyAccess().getAccessToken());

        Events events;
        com.google.api.services.calendar.model.Calendar calendar;
        try {
            calendar = service.calendars().get(calendarId).execute();
            events = service.events().list(calendarId).execute();
        } catch (IOException e) {
            throw new InternalException("Problem retrieving calendar", e);
        }


        return GoogleCalendar.from(calendar, events);
    }
}
