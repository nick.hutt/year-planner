package com.gitlab.nickhutt.yearplanner.googlecalendar.model;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import org.immutables.value.Value;
import org.springframework.lang.Nullable;

@Value.Immutable
public interface GoogleEvent extends Event {

    @Nullable
    @Override
    String getTitle();

    @Nullable
    @Override
    String getDescription();

    @Nullable
    @Override
    String getColor();

    static GoogleEvent from(com.google.api.services.calendar.model.Event event) {
        return ImmutableGoogleEvent.builder()
                .id(event.getId())
                .title(event.getSummary())
                .description(event.getDescription())
                .start(GoogleTemporalPoint.from(event.getStart()))
                .end(GoogleTemporalPoint.from(event.getEnd()))
                .color(event.getColorId())
                .build();
    }
}
