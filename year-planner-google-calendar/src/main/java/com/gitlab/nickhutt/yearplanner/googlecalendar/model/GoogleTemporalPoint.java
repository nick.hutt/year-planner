package com.gitlab.nickhutt.yearplanner.googlecalendar.model;

import com.gitlab.nickhutt.yearplanner.domain.calendar.TemporalPoint;
import com.google.api.services.calendar.model.EventDateTime;
import org.immutables.value.Value;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.time.OffsetDateTime;

@Value.Immutable
public interface GoogleTemporalPoint extends TemporalPoint {

    @Nullable
    @Override
    OffsetDateTime getDatetime();

    @Nullable
    @Override
    LocalDate getDate();

    static GoogleTemporalPoint from(EventDateTime eventDateTime) {

        return ImmutableGoogleTemporalPoint.builder()
                .date(convertDate(eventDateTime))
                .datetime(convertDateTime(eventDateTime))
                .build();
    }

    static OffsetDateTime convertDateTime(EventDateTime eventDateTime) {
        if (eventDateTime == null || eventDateTime.getDateTime() == null) {
            return null;
        }

        return OffsetDateTime.parse(eventDateTime.getDateTime().toString());
    }

    static LocalDate convertDate(EventDateTime eventDateTime) {
        if (eventDateTime == null || eventDateTime.getDate() == null) {
            return null;
        }

        return LocalDate.parse(eventDateTime.getDate().toString());
    }
}
