package com.gitlab.nickhutt.yearplanner.googlecalendar.accesstoken;

import com.gitlab.nickhutt.yearplanner.domain.UnableToAuthenticateUserException;
import com.gitlab.nickhutt.yearplanner.googlecalendar.GoogleClientConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hobsoft.spring.resttemplatelogger.LoggingCustomizer;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
@Slf4j
public class GoogleAccessTokenClient {

    private final GoogleClientConfiguration configuration;

    public String getAccessTokenFromCode(String code) {
        RestTemplate restTemplate = new RestTemplateBuilder()
                .customizers(new LoggingCustomizer())
                .build();

        GoogleAccessTokenRequest googleAccessTokenRequest = getGoogleAccessTokenRequestHttpEntity(code);
        HttpEntity<GoogleAccessTokenRequest> request = new HttpEntity<>(googleAccessTokenRequest);
        GoogleAccessTokenResponse response;
        try {
            response = restTemplate.postForObject(configuration.getUrl(), request, GoogleAccessTokenResponse.class);
        } catch (HttpClientErrorException.BadRequest e) {
            // TODO should differentiate between internal errors and user error
            log.warn("Could not authenticate", e);
            throw new UnableToAuthenticateUserException("Unable to authenticate: " + e.getMessage());
        }

        return response.getAccessToken();
    }

    private GoogleAccessTokenRequest getGoogleAccessTokenRequestHttpEntity(String code) {
        GoogleAccessTokenRequest googleAccessTokenRequest = new GoogleAccessTokenRequest();
        googleAccessTokenRequest.setGrantType("authorization_code");
        googleAccessTokenRequest.setRedirectUri(configuration.getRedirectUri());
        googleAccessTokenRequest.setClientId(configuration.getClientId());
        googleAccessTokenRequest.setClientSecret(configuration.getClientSecret());
        googleAccessTokenRequest.setCode(code);
        return googleAccessTokenRequest;
    }

}
