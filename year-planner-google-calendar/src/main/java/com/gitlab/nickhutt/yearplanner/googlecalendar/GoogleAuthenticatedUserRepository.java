package com.gitlab.nickhutt.yearplanner.googlecalendar;

import com.gitlab.nickhutt.yearplanner.domain.user.AuthenticatedUserRepository;
import com.gitlab.nickhutt.yearplanner.domain.user.User;
import com.gitlab.nickhutt.yearplanner.domain.user.UserAggregateRoot;
import com.gitlab.nickhutt.yearplanner.googlecalendar.accesstoken.GoogleAccessTokenClient;
import com.gitlab.nickhutt.yearplanner.googlecalendar.calendar.GoogleCalendarClient;
import com.gitlab.nickhutt.yearplanner.googlecalendar.model.GoogleAuthenticatedUser;
import com.gitlab.nickhutt.yearplanner.googlecalendar.model.GoogleCalendar;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GoogleAuthenticatedUserRepository implements AuthenticatedUserRepository {

    private final GoogleAccessTokenClient googleAccessTokenClient;

    private final UserAggregateRoot userAggregateRoot;

    private final GoogleCalendarClient googleCalendarClient;

    // TODO need to get the username
    @Override
    public User findUserFromCode(String code) {
        String accessToken = googleAccessTokenClient.getAccessTokenFromCode(code);
        User user = userAggregateRoot.create("TODO", accessToken, "Google Calendar");
        List<GoogleCalendar> calendars = googleCalendarClient.getCalendarIds(user);
        return GoogleAuthenticatedUser.from(user, calendars);
    }
}
