package com.gitlab.nickhutt.yearplanner.googlecalendar.model;

import com.gitlab.nickhutt.yearplanner.domain.user.User;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
public interface GoogleAuthenticatedUser extends User {
    static GoogleAuthenticatedUser from(User user, List<GoogleCalendar> calendars) {
        return ImmutableGoogleAuthenticatedUser.builder()
                .from(user)
                .addAllCalendars(calendars)
                .build();
    }
}
