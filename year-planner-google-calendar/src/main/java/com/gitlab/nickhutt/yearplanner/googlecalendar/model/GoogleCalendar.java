package com.gitlab.nickhutt.yearplanner.googlecalendar.model;

import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Events;
import org.immutables.value.Value;

import java.util.List;
import java.util.stream.Collectors;

@Value.Immutable
public interface GoogleCalendar extends Calendar {

    static GoogleCalendar from(CalendarListEntry entry) {
        return ImmutableGoogleCalendar.builder()
                .id(entry.getId())
                .name(entry.getSummary())
                .build();
    }

    static GoogleCalendar from(com.google.api.services.calendar.model.Calendar calendar, Events events) {
        List<GoogleEvent> googleEvents = events.getItems().stream()
                .map(item -> GoogleEvent.from(item))
                .collect(Collectors.toList());

        return ImmutableGoogleCalendar.builder()
                .id(calendar.getId())
                .name(calendar.getSummary())
                .addAllEvents(googleEvents)
                .build();
    }
}
