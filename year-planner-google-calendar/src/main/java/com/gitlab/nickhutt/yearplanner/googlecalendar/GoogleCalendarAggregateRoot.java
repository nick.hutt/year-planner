package com.gitlab.nickhutt.yearplanner.googlecalendar;

import com.gitlab.nickhutt.yearplanner.domain.InternalException;
import com.gitlab.nickhutt.yearplanner.domain.calendar.Calendar;
import com.gitlab.nickhutt.yearplanner.domain.calendar.CalendarAggregateRoot;
import com.gitlab.nickhutt.yearplanner.domain.calendar.Event;
import com.gitlab.nickhutt.yearplanner.domain.user.User;
import com.gitlab.nickhutt.yearplanner.googlecalendar.calendar.GoogleCalendarClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service("google")
@RequiredArgsConstructor
public class GoogleCalendarAggregateRoot implements CalendarAggregateRoot {

    private final GoogleCalendarClient googleCalendarClient;

    @Override
    public Calendar findById(User user, String calendarId) {
        return googleCalendarClient.findById(user, calendarId);
    }

    @Override
    public Calendar addEvent(User user, String calendarId, Event event) {
        throw new InternalException("Add event currently not supported");
    }

    @Override
    public Calendar updateEvent(User user, String calendarId, String eventId, Event event) {
        throw new InternalException("Add event currently not supported");
    }

    @Override
    public Calendar deleteEvent(User user, String calendarId, String eventId) {
        throw new InternalException("Add event currently not supported");
    }
}
